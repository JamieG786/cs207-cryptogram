# CS207 Cryptogram

This is a program created by Maria Elizabeth Hughes, Jamie Greenaway, Nathan Goutcher and Ross Mitchell as a coursework for the class CS207 at the University of Strathclyde.
It is a program in Java which allows the user to solve cryptograms. The functionality of this program is done from the command line.
A user can create an account and start solving cryptograms. Their stats are then saved and stored in leaderboards which are available for the user to view.
When solving a cryptogram the user has the ability to see the letter frequencies, undo actions and recieve up to 5 hints to help them solve the puzzle.
The player can also choose to save a game if they wish to come back to it.

Please refer to the documents for an in depth design.

![](CryptogramScreenshot.png)