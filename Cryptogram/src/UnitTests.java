import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTests {

    @Test
    public void encryptAlphabetTest() {
        Cryptogram c = new Cryptogram("test");
        for(int i = 0; i < c.getEncAlphabet().size(); i++) {
            assertNotEquals(c.getEncAlphabet().get(i), c.getDecAlphabet().get(i));
        }
    }

    @Test
    public void encryptMessageTest() {
        Cryptogram c = new Cryptogram("test");
        assertNotEquals(c.getEncMessage(), "test");
    }

    @Test
    public void undoTest() {
        Player p = new Player("test",0,0,0,0);
        Cryptogram c = new Cryptogram("test");
        char[] testEncMsgArr = c.getEncMessage();
        char[] testUsrAtmpt1 = c.getUserAttempt();
        c.setEncryptedChar(testEncMsgArr[0], testEncMsgArr[2], p);
        char[] testUsrAtmpt2 = c.getUserAttempt();
        assertNotEquals(testUsrAtmpt1, testUsrAtmpt2);
        c.undo();
        testUsrAtmpt2 = c.getUserAttempt();
        assertEquals(testUsrAtmpt1, testUsrAtmpt2);

    }
    @Test
    public void saveStatsTest() throws IOException {
        Player p = new Player("test1", 0, 0, 0, 0);
        Players.allPlayers.clear();
        Players.allPlayers.add(p);
        Players.savePlayers("src/statsTest.txt");
        BufferedReader br = new BufferedReader(new FileReader("src/statsTest.txt"));
        String line = br.readLine();
        Scanner sc = new Scanner(line);
        String testName = sc.next();
        assertEquals("test1", testName);
    }

    @Test
    public void hintTest() {
       Player p = new Player("test1", 0, 0, 0, 0);
       Game testGame = new Game(p, "test");
       assert(testGame.isComplete());
    }

    @Test
    public void loadStatsTest() {
        Player p = new Player("test2", 0, 0, 0, 0);
        Players.allPlayers.clear();
        Players.allPlayers.add(p);
        Players.savePlayers("src/loadStatsTest.txt");
        Player testPlayer = Players.findPlayer("test2");
        assert (testPlayer.getUserName().equals("test2"));
    }

    @Test
    public void correctGuessesTest(){
        Player p = new Player("test",0,0,0,0);
        Cryptogram c = new Cryptogram("test");
        c.setEncryptedChar(c.getEncMessage()[0], 'g' , p);
        assert(p.getTotalGuesses() == 1);
        assert(p.getCorrectGuesses() == 0);
        c.setEncryptedChar(c.getEncMessage()[0], 't', p);
        assert(p.getTotalGuesses() == 2);
        assert(p.getCorrectGuesses() == 1);
        assert(p.getAccuracy() == 50.0);
    }

    @Test
    public void cryptogramsPlayedTest() {
        Player p = new Player("test",0,0,0,0);
        Game g = new Game(p, "t", "");
        assertEquals(1, p.getNumCryptogramsPlayed());
    }

    @Test
    public void cryptogramsCompletedTest() {
        Player p = new Player("test",0,0,0,0);
        Game g = new Game(p, "t");
        assertEquals(1, p.getNumCryptogramsCompleted());
    }

    @Test
    public void enterLetterTest() {
        Player p = new Player("test", 0,0,0,0);
        Game g = new Game(p, "test");

        int testInputCode = g.handleInput("e");
        assertEquals(0, testInputCode);

        testInputCode = g.handleInput("test");
        assertEquals(testInputCode, 2);
    }

    @Test
    public void saveAndLoadGameTest() {
        Player p = new Player("test", 0,0,0,0);
        Game g = new Game(p, "test");

        g.saveGame();

        Game g2 = new Game(p, true);

        assertEquals(Arrays.toString(g.c.getEncMessage()), Arrays.toString(g2.c.getEncMessage()));
        assertEquals(Arrays.toString(g.c.getUserAttempt()), Arrays.toString(g2.c.getUserAttempt()));
    }

    @Test
    public void removeSaveGameTest() {
        //This is used to test giveUp feature
        Player p = new Player("test", 0,0,0,0);
        Game g = new Game(p, "test");

        g.saveGame();

        assertTrue(g.removeSaveGame());

        Game g2 = new Game(p, "test");
        assertFalse(g2.removeSaveGame());
    }
}
