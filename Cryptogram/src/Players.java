import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Players {

    public static ArrayList<Player> allPlayers = new ArrayList<>();

    public static void createAllPlayers(String fileName) {
        String userName;
        int totalGuesses;
        int correctGuesses;
        int played;
        int completed;
        try {
            // FileReader reads text files in the default encoding.
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            // Always wrap FileReader in BufferedReader.
            String line;
            while ((line = br.readLine()) != null) {
                Scanner scan = new Scanner(line);
                userName = scan.next();
                totalGuesses = scan.nextInt();
                correctGuesses = scan.nextInt();
                played = scan.nextInt();
                completed = scan.nextInt();
                Player player = new Player(userName, totalGuesses, correctGuesses, played, completed);
                allPlayers.add(player);
            }
            // Always close files.
            br.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file playerStats.txt'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file playerStats.txt");
        }
    }

    protected static void addPlayer(Player p) {
        try (FileWriter w = new FileWriter("src/playerStats.txt", true);
             BufferedWriter bw = new BufferedWriter(w)) {
            bw.write(p.toString());
            bw.newLine();
            bw.close();
        } catch (FileNotFoundException e) {
            System.out.println("couldn't find file");

        } catch (IOException e) {
            System.out.println("prob writing line");
        }
    }

    public static Player findPlayer(String currentName) {
        for (Player p : allPlayers) {
            if (p.getUserName().equals(currentName)) {
                System.out.println("Welcome back!");
                return p;
            }
        }
        return null;
    }

    protected static void savePlayers(String fileName) {
        try (FileWriter w = new FileWriter(fileName);
             BufferedWriter bw = new BufferedWriter(w)) {
            for (Player p : allPlayers) {
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            System.out.println("prob writing line");
        }
    }

    public static void removePlayer(String name) {
        ArrayList<Player> tempPlayers = new ArrayList<>();
        for (Player p : allPlayers) {
            if (!p.getUserName().equals(name)) {
                tempPlayers.add(p);
            }
        }
        try (FileWriter w = new FileWriter("src/playerStats.txt");
             BufferedWriter bw = new BufferedWriter(w)) {
            for (Player p : tempPlayers) {
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            System.out.println("prob writing line");
        }
    }

    public static void createLeaderboard() {
        ArrayList<Player> copyPlayers = allPlayers;
        String[] sortedPlayers = new String[copyPlayers.size()];
        Player bestPlayer = null;
        System.out.println("Rank\tUsername\t\tCryptograms Completed");
        for (int i =0; i < 10; i++) {
            if(i == sortedPlayers.length)
                break;
            int bestScore = -1;
            for (Player p: copyPlayers) {
                if (p.getNumCryptogramsCompleted() > bestScore) {
                    bestScore = p.getNumCryptogramsCompleted();
                    bestPlayer = p;
                }
            }
            sortedPlayers[i] = i+1 + "\t\t" + bestPlayer.getUserName() + "     \t\t\t\t\t\t" + bestPlayer.getNumCryptogramsCompleted();
            copyPlayers.remove(bestPlayer);
            System.out.println(sortedPlayers[i]);
        }
        System.out.println(" ");
    }
}


