import java.util.Scanner;

//This class is the first class that will be run when the program is started.
public class Driver {

    private static Player currentPlayer;
    private static Scanner scanner = new Scanner(System.in);
    //This function will display the welcome message before calling the menu function
    public static void main(String[] args) {
        Players.createAllPlayers("src/playerStats.txt");
        welcome();
    }

    public static void welcome() {
        System.out.println("Welcome to the cryptogram game! Please enter your username (max 6 characters): \n");

        String name = scanner.next();
        while(name.length() > 6) {
            System.out.println("Username cannot be over 6 characters. Try again.");
            name = scanner.next();
        }
        currentPlayer = Players.findPlayer(name);
        if (currentPlayer == null) {
            currentPlayer = new Player(name, 0, 0, 0, 0);
            Players.allPlayers.add(currentPlayer);
            Players.addPlayer(currentPlayer);
        }
        menu();
    }
    //This function will present the user with all the different commands
    //and deal with requests in the correct manner
    public static void menu() {
        System.out.println("Menu, please type: \n'new' - start a new game" +
                "\n'leaderboard' - show the current top 10 players" +
                "\n'load' - load a previous game\n'stats' - see your statistics" +
                "\n'remove' - remove your account details (and terminate)" +
                "\n'exit' - close the program");
        String userInput = scanner.next().toLowerCase();
        while(true) {
            switch (userInput) {
                case "new":
                    Game game = new Game(currentPlayer);
                    menu();
                case "leaderboard":
                    Players.createLeaderboard();
                    menu();
                case "load":
                    game = new Game(currentPlayer, true);
                    menu();
                case "help":
                    System.out.println("Not implemented yet");
                    menu();
                case "stats":
                    System.out.println(currentPlayer.getStats() + "\n");
                    menu();
                case "exit":
                    Players.savePlayers("src/playerStats.txt");
                    System.exit(0);
                case "remove":
                    Players.removePlayer(currentPlayer.getUserName());
                    System.out.println("Player stats removed. Terminating...");
                    System.exit(0);
                default:
                    System.out.println("Invalid Input");
                    menu();

            }
        }
    }
}


