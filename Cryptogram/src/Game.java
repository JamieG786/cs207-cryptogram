import java.io.*;
import java.util.*;

//This is the class which will run the game.
public class Game {

    public Cryptogram c;
    private Player currentPlayer;



    //This method will start the game loop and call all the appropriate functions for the game to run correctly
    public Game(Player p) {
        currentPlayer = p;
        currentPlayer.incrementCryptogramsPlayed();
        System.out.println("Game starting.."); //take out path
        ArrayList<String> msgList = readMsgTxtFile("src/test.txt");

        Random rand = new Random();
        String message = msgList.get(rand.nextInt(msgList.size()));

        if(message != "" && message != null) {
            c = new Cryptogram(message.toLowerCase());
            System.out.println("Encrypted Message:");
            System.out.println(c.getEncMessage());
            System.out.println(c.getUserAttempt());
            while(!isComplete()){
                System.out.println("Enter a letter to guess or a game command.");
                char letterToChange = enterLetter();

                if(letterToChange == '1')
                    continue;

                boolean found = false;
                char[] temp = c.getEncMessage();

                for(int i = 0; i < c.getEncMessage().length; i++) {
                    if(temp[i] == letterToChange)
                        found = true;
                }

                if(!found) {
                    System.out.println("That letter does not exist in the encrypted message.");
                    continue;
                }

                System.out.println("Which letter do you think it is?");
                char newLetter = enterLetter();

                if(newLetter == '1')
                    continue;

                c.setEncryptedChar(letterToChange, newLetter, currentPlayer);
                System.out.println("Encrypted Message:");
                System.out.println(c.getEncMessage());
                System.out.println(c.getUserAttempt());
                System.out.println(" ");
            }

            System.out.println("Congratulations. You have completed this puzzle!");
        } else {
            System.out.println("File is empty.");
        }
    }

    public Game(Player p, boolean load) {
        currentPlayer = p;

        String fileName = "src/games.txt";
        try {
            // FileReader reads text files in the default encoding.
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            // Always wrap FileReader in BufferedReader.
            String line;
            String username;
            String decMsg = null;
            String usrAttmpt = null;
            String encAlphabet = null;
            while ((line = br.readLine()) != null) {
                Scanner scan1 = new Scanner(line);
                username = scan1.next();
                if(username.equals(currentPlayer.getUserName())) {
                    encAlphabet = scan1.next();
                    decMsg = scan1.next();
                    usrAttmpt = scan1.next();
                } else {
                    continue;
                }
            }
            // Always close files.
            br.close();

            if(decMsg != "" && decMsg != null) {
                System.out.println("Game starting..");
                c = new Cryptogram(decMsg.toLowerCase().replace("/", " "),
                        encAlphabet.toCharArray(), usrAttmpt.replace("/", " ").toCharArray());

                System.out.println("Encrypted Message:");
                System.out.println(c.getEncMessage());
                System.out.println(c.getUserAttempt());
                while(!isComplete()){
                    System.out.println("Enter a letter to guess or a game command.");
                    char letterToChange = enterLetter();

                    if(letterToChange == '1')
                        continue;

                    boolean found = false;
                    char[] temp = c.getEncMessage();

                    for(int i = 0; i < c.getEncMessage().length; i++) {
                        if(temp[i] == letterToChange)
                            found = true;
                    }

                    if(!found) {
                        System.out.println("That letter does not exist in the encrypted message.");
                        continue;
                    }

                    System.out.println("Which letter do you think it is?");
                    char newLetter = enterLetter();

                    if(newLetter == '1')
                        continue;

                    c.setEncryptedChar(letterToChange, newLetter, currentPlayer);
                    System.out.println("Encrypted Message:");
                    System.out.println(c.getEncMessage());
                    System.out.println(c.getUserAttempt());
                    System.out.println(" ");
                }

                System.out.println("Congratulations. You have completed this puzzle!");
            } else {
                System.out.println("There are no saves for this username.");
            }
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file playerStats.txt'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file playerStats.txt");
        }
    }

    //This is a test constructor which completes
    public Game(Player p, String message) {
        currentPlayer = p;
        c = new Cryptogram(message);
        c.encryptString(message);
        while (!isComplete()) {
           c.hint();
       }
    }

    public Game(Player p, String message, String option) {
        p.incrementCryptogramsPlayed();
        currentPlayer = p;
        c = new Cryptogram(message);
        c.encryptString(message);
        c.setEncryptedChar(c.getEncMessage()[0], 't', p);
        isComplete();
    }

    // This method reads a message from a file to be encrypted
    public ArrayList readMsgTxtFile(String fileName) {
        ArrayList<String> messagesList = new ArrayList<>();
        String message = null;
        String line;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                message = line;
                messagesList.add(message);
            }

            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        return messagesList;
    }
    //This method is getting the user input and dealing with it in the correct manner
    public char enterLetter() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next().toLowerCase();

        int inputCode = handleInput(input);

        switch(inputCode) {
            case 0 :
                char[] l = input.toCharArray();
                return l[0];
            case 1 :
                return '1';
            case 2 :
                return enterLetter();
        }
        return '3';
    }

    //This handles the input from the user, and calls the functions corresponding to the input
    //It will also check if the input is valid
    public int handleInput(String input) {

        switch(input) {
            case "exit" :
                Players.savePlayers("src/playerStats.txt");
                System.out.println("Terminating..");
                System.exit(0);
            case "frequency":
                System.out.println("Letter\tFrequency\t\tPercentage\t\tEnglish Letter Frequencies");
                for (int i=0; i < 26; i++) {

                    System.out.println(c.getEncAlphabet().get(i) +"\t\t\t"+ c.generateFrequencies()[i]+ "\t\t\t" + c.generateFrequencyPercentage(c.generateFrequencies())[i] + "%\t\t\t\t\t" +c.generateEnglishFrequencies()[i] );
                }
                return 1;
            case "undo" :
                c.undo();
                return 1;
            case "stats" :
                System.out.println(currentPlayer.getStats());
                return 1;
            case "save":
                System.out.println("This will overwrite any previous save. Continue? [y/n]");
                char choice = enterLetter();
                if(choice == 'y') {
                    saveGame();
                    System.out.println("Save successful");
                }
                else if(choice == 'n')
                    System.out.println("Saving aborted..");
                else {
                    System.out.println("Unexpected input. Resuming game..");
                }
                return 1;
            case "menu":
                Driver.menu();
                return 1;
            case "giveup":
                System.out.println("This will show the solution, quit to menu and also remove any saves for this cryptogram. Continue? [y/n]");
                choice = enterLetter();
                if(choice == 'y') {
                    System.out.println("The solution is:\n" + c.getDecMessage());
                    removeSaveGame();
                    Driver.menu();
                    return 1;
                } else if (choice == 'n') {
                    System.out.println("Give up aborted.");
                } else {
                    System.out.println("Unexpected input. Resuming game..");
                }
                return 1;
            case "hint" :
                c.hint();
                return 1;
            default:
                if(input.length() > 1) {
                System.out.println("Invalid input. Expected 1 letter or a command. Try again.");
                return 2;
            }
        }

        //Checks that input is a letter
        if(c.getEncAlphabet().contains(input.charAt(0)))
            return 0;
        else {
            System.out.println("Invalid input. Expected 1 letter. Try again.");
            return 2;
        }
    }


    //This function checks if the cryptogram is complete. It compares the user attempt to the decrypted message
    //It returns true if the users attempt is correct and false if not
    public boolean isComplete() {
        String attempt = new String(c.getUserAttempt());
        if(attempt.equals(c.getDecMessage())) {
            currentPlayer.incrementCryptogramsCompleted();
            return true;
        }
        return false;
    }

    public void saveGame() {
        List<String> saves = new ArrayList<>();

        //Read in all lines into saves arraylist
        try {
            // FileReader reads text files in the default encoding.
            BufferedReader br = new BufferedReader(new FileReader("src/games.txt"));
            // Always wrap FileReader in BufferedReader.
            String line;
            while ((line = br.readLine()) != null) {
                Scanner scan = new Scanner(line);
                saves.add(line);
            }
            // Always close files.
            br.close();
        } catch (IOException ex) {
            //Empty catch statements as we don't want program to crash but also don't want a user output at this point
        }

        //Remove any saves with the current username
        for(int i = 0; i < saves.size(); i++) {
            if(saves.get(i).contains(currentPlayer.getUserName())) {
                saves.remove(i);
            }
        }

        char[] tempArr = new char[c.getEncAlphabet().size()];

        for(int i = 0; i < c.getEncAlphabet().size(); i++) {
            tempArr[i] = c.getEncAlphabet().get(i);
        }

        //Adds new save
        String details = currentPlayer.getUserName() + " "
                + Arrays.toString(tempArr).replace(" ", "")
                + " " + Arrays.toString(c.getDecMessage().toCharArray()).replace("  ", "/").replace(" ", "")
                + " " + Arrays.toString(c.getUserAttempt()).replace("  ", "/").replace(" ", "");
        saves.add(details);

        //Writes to file
        try (FileWriter w = new FileWriter("src/games.txt");
             BufferedWriter bw = new BufferedWriter(w)) {
            for (String save : saves) {
                String temp = save.replace("[", "").replace("]", "").replace(",", "");
                bw.write(temp);
                bw.newLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("couldn't find file");

        } catch (IOException e) {
            System.out.println("prob writing line");
        }
    }

    //removes save game for current cryptogram if user gives up
    public boolean removeSaveGame() {
        List<String> saves = new ArrayList<>();

        //Read in all lines into saves arraylist
        try {
            // FileReader reads text files in the default encoding.
            BufferedReader br = new BufferedReader(new FileReader("src/games.txt"));
            // Always wrap FileReader in BufferedReader.
            String line;
            while ((line = br.readLine()) != null) {
                saves.add(line);
            }
            // Always close files.
            br.close();
        } catch (IOException ex) {
            //Empty catch statements as we don't want program to crash but also don't want a user output at this point
        }

        //Used for testing purposes
        boolean found = false;

        //Remove any saves with the current username
        for (int i = 0; i < saves.size(); i++) {
            if (saves.get(i).contains(currentPlayer.getUserName()) && saves.get(i).replace("/", " ").contains(c.getDecMessage())) {
                saves.remove(i);
                found = true;
            }
        }

        //Writes to file
        try (FileWriter w = new FileWriter("src/games.txt");
             BufferedWriter bw = new BufferedWriter(w)) {
            for (String save : saves) {
                String temp = save.replace("[", "").replace("]", "").replace(",", "");
                bw.write(temp);
                bw.newLine();
            }
            return found;
        } catch (IOException e) {
            return found;
            //Empty catch statements as we don't want program to crash but also don't want a user output at this point
        }
    }


}

