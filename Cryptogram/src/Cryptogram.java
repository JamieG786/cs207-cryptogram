import java.util.ArrayList;
import java.util.Random;

public class Cryptogram {

    //Decrypted letters (alphabet)
    private ArrayList<Character> decAlphabet = new ArrayList<>();
    private ArrayList<Integer> decAlphabetIndex = new ArrayList<>();

    //Encrypted version of decAlphabet
    private ArrayList<Character> encAlphabet;
    private ArrayList<Integer> encAlphabetIndex;

    //ArrayList of encrypted arrays
    private ArrayList<char[]> encUserGuessArray = new ArrayList<>();

    //Encrypted version of message
    private char[] encMessage;

    //Decrypted message
    private String decMessage;

    //User's attempt
    private char[] userAttempt;


    public Cryptogram(String message) {

        //Adds the alphabet to decAlphabet
        for (char c = 'a'; c <= 'z'; c++) {
            decAlphabet.add(c);
        }
        for (int i = 0; i < decAlphabet.size(); i++) {
            decAlphabetIndex.add(i);
        }

        decMessage = message;

        encAlphabet = encryptAlphabet();
        /*System.out.println("Decrypted Alphabet index: ");
        System.out.println(decAlphabetIndex);

        System.out.println("Decrypted Alphabet: ");
        System.out.println(decAlphabet + "\n");

        System.out.println("Encrypted Alphabet Index: ");
        System.out.println(encAlphabetIndex);

        System.out.println("Encrypted Alphabet: ");
        System.out.println(encAlphabet + "\n");

        System.out.println("Decrypted message: ");
        System.out.println(message.toLowerCase() + "\n");

        System.out.println("Decrypted: ");
        for(int i = 0; i < decAlphabet.size(); i++){
            System.out.println(decAlphabet.get(i) + ", " + decAlphabetIndex.get(i));
        }

        System.out.println("Encrypted: ");
        for(int i = 0; i < encAlphabet.size(); i++){
            System.out.println(encAlphabet.get(i) + ", " + encAlphabetIndex.get(i));
        }*/

        encMessage = encryptString(message);

        userAttempt = new char[encMessage.length];
        //Fills the empty letters with '-'
        for (int i = 0; i < userAttempt.length; i++) {
            if (encMessage[i] == ' ' || encMessage[i] == '.') {
                userAttempt[i] = encMessage[i];
            } else {
                userAttempt[i] = '-';
            }
        }

        encUserGuessArray.add(userAttempt);
    }

    public Cryptogram(String message, char[] encAlphabet, char[] userAttmpt) {
        //Adds the alphabet to decAlphabet
        for (char c = 'a'; c <= 'z'; c++) {
            decAlphabet.add(c);
        }
        for (int i = 0; i < decAlphabet.size(); i++) {
            decAlphabetIndex.add(i);
        }
        decMessage = message;

        this.encAlphabet = new ArrayList<>();

        for (int i = 0; i < encAlphabet.length; i++) {
            this.encAlphabet.add(encAlphabet[i]);
        }

        /*encAlphabetIndex = mapNumbers();
        System.out.println("Decrypted Alphabet index: ");
        System.out.println(decAlphabetIndex);

        System.out.println("Decrypted Alphabet: ");
        System.out.println(decAlphabet + "\n");

        System.out.println("Encrypted Alphabet Index: ");
        System.out.println(encAlphabetIndex);

        System.out.println("Encrypted Alphabet: ");
        System.out.println(this.encAlphabet + "\n");

        System.out.println("Decrypted message: ");
        System.out.println(message.toLowerCase() + "\n");

        System.out.println("Decrypted: ");
        for(int i = 0; i < decAlphabet.size(); i++){
            System.out.println(decAlphabet.get(i) + ", " + decAlphabetIndex.get(i));
        }

        System.out.println("Encrypted: ");
        for(int i = 0; i < encAlphabet.size(); i++){
            System.out.println(encAlphabet.get(i) + ", " + encAlphabetIndex.get(i));
        }*/

        encMessage = encryptString(message);

        this.userAttempt = userAttmpt;

        encUserGuessArray.add(userAttempt);
    }


    //This method encrypts the alphabet
    public ArrayList encryptAlphabet() {
        ArrayList<Character> encryptedArray = new ArrayList<>();
        encAlphabetIndex = new ArrayList<>();

        Random rand = new Random();

        int index = 0;
        while (index < 26) {
            int n = rand.nextInt(26);
            while (!encryptedArray.contains(decAlphabet.get(n)) && !encAlphabetIndex.contains(decAlphabetIndex.get(n))) {
                encryptedArray.add(decAlphabet.get(n));
                encAlphabetIndex.add(decAlphabetIndex.get(n));
                index++;
            }
        }

        for (int i = 0; i < encryptedArray.size(); i++) {
            if (encryptedArray.get(i).equals(decAlphabet.get(i))) {
                return encryptAlphabet();
            }
        }

        return encryptedArray;
    }

    //This method will encrypt the message to be solved
    public char[] encryptString(String message) {
        char[] encryptedArray = message.toLowerCase().toCharArray();
        char[] tempArr = new char[encryptedArray.length];
        for (int i = 0; i < encryptedArray.length; i++) {
            for (int j = 0; j < decAlphabet.size(); j++) {
                if (decAlphabet.get(j).equals(encryptedArray[i])) {
                    tempArr[i] = encAlphabet.get(j);
                } else if (encryptedArray[i] == ' ' || encryptedArray[i] == '!' || encryptedArray[i] == '.'
                        || encryptedArray[i] == ',' || encryptedArray[i] == '?' || encryptedArray[i] == '\'') {
                    tempArr[i] = encryptedArray[i];
                }
            }
        }
        return tempArr;
    }

    //This method handles the user attempt in retrospect to the cryptogram
    public void setEncryptedChar(char toChange, char newLetter, Player currentPlayer) {
        char[] tempEncMsgArr = new char[getEncMessage().length];
        char[] tempEncMsgArr2 = getEncMessage();
        for (int j = 0; j < tempEncMsgArr.length; j++) {
            tempEncMsgArr[j] = tempEncMsgArr2[j];
        }
        char[] tempUsrAttempt = new char[getUserAttempt().length];
        char[] tempUsrAttempt2 = getUserAttempt();
        for (int i = 0; i < tempUsrAttempt.length; i++) {
            tempUsrAttempt[i] = tempUsrAttempt2[i];
        }
        int index = 0;
        for (int i = 0; i < tempEncMsgArr.length; i++) {
            if (tempEncMsgArr[i] == toChange) {
                tempUsrAttempt[i] = newLetter;
                index = i;
            }
        }
        currentPlayer.incrementTotalGuesses();
        if (tempUsrAttempt[index] == decMessage.toCharArray()[index]) currentPlayer.incrementCorrectGuesses();
        encUserGuessArray.add(tempUsrAttempt);
    }

    //This is the method that will undo the users last attempt
    public void undo() {
        if (encUserGuessArray.size() > 1) {
            encUserGuessArray.remove(getUserAttempt());

            System.out.println(getEncMessage());
            System.out.println(getUserAttempt());
            System.out.println(" ");
        } else {
            System.out.println("Cannot undo");
        }

    }

    public void hint() {
        boolean found = false;
        int length = encMessage.length;
        Random rand = new Random();
        int num = rand.nextInt(length);

        while (found == false) {
            if (getUserAttempt()[num] != decMessage.charAt(num)) {
                getUserAttempt()[num] = decMessage.charAt(num);
                found = true;

                for(int i = 0; i < length; i++) {
                    if (decMessage.charAt(i) == decMessage.charAt(num)) {
                        getUserAttempt()[i] = decMessage.charAt(i);
                    }
                }
            } else {
                num = rand.nextInt(length);
            }
        }

        System.out.println("Encrypted Message:");
        System.out.println(getEncMessage());
        System.out.println(getUserAttempt());
        System.out.println(" ");
    }

    public ArrayList<Integer> mapNumbers() {
        ArrayList<Integer> encIndex = new ArrayList<>();
        int count = 0;
        while (encIndex.size() < 26) {
            char toSearch = encAlphabet.get(count);
            for (int i = 0; i < decAlphabetIndex.size(); i++) {
                if (encAlphabet.get(i).equals(toSearch)) {
                    encIndex.add(decAlphabet.indexOf(toSearch));
                }
            }
            count++;
        }
        return encIndex;
    }

    // Get methods
    public ArrayList<Character> getEncAlphabet() {
        return encAlphabet;
    }

    public ArrayList<Character> getDecAlphabet() {
        return decAlphabet;
    }

    public char[] getUserAttempt() {
        return encUserGuessArray.get(encUserGuessArray.size() - 1);
    }

    public char[] getEncMessage() {
        return encMessage;
    }

    public String getDecMessage() {
        return decMessage;
    }

    public int[] generateFrequencies() {
        //ArrayList<Integer> letterFrequencies = new ArrayList<>();
        int[] letterFrequencies = new int[26];
        int count=0;
        int j=0;
        for (Character c: encAlphabet) {
            count = 0;
            for (Character d: encMessage) {
                if (c == d) {
                    count++;
                }
            }
            letterFrequencies[j] = count;
            j++;
        }
        return letterFrequencies;

    }

    public int[] generateFrequencyPercentage(int[] letterFrequencies){
        int[] letterPercentage = new int[26];
        int count=0;
        for(int l: letterFrequencies){
            if (l > 0){
                count++;
            }
        }
        int j=0;
        for (int k: letterFrequencies){
            letterPercentage[j] = k*100/count;
            j++;
        }
        return letterPercentage;
    }

    public String[] generateEnglishFrequencies(){
        final String[] engFrequency = new String[26];

        engFrequency[0]= "E\t\t\t12.02%";
        engFrequency[1]= "T\t\t\t9.10%";
        engFrequency[2]= "A\t\t\t8.12%";
        engFrequency[3]= "O\t\t\t7.68%";
        engFrequency[4]= "1\t\t\t7.31%";
        engFrequency[5]= "N\t\t\t6.96%";
        engFrequency[6]= "S\t\t\t6.28%";
        engFrequency[7]= "R\t\t\t6.02%";
        engFrequency[8]= "H\t\t\t5.92%";
        engFrequency[9]= "D\t\t\t4.32%";
        engFrequency[10]= "L\t\t\t3.98%";
        engFrequency[11]= "U\t\t\t2.88%";
        engFrequency[12]= "C\t\t\t2.71%";
        engFrequency[13]= "M\t\t\t2.61%";
        engFrequency[14]= "F\t\t\t2.30%";
        engFrequency[15]= "Y\t\t\t2.11%";
        engFrequency[16]= "W\t\t\t2.09%";
        engFrequency[17]= "G\t\t\t2.03%";
        engFrequency[18]= "P\t\t\t1.82%";
        engFrequency[19]= "B\t\t\t1.49%";
        engFrequency[20]= "V\t\t\t1.11%";
        engFrequency[21]= "K\t\t\t0.69%";
        engFrequency[22]= "X\t\t\t0.17%";
        engFrequency[23]= "Q\t\t\t0.11%";
        engFrequency[24]= "J\t\t\t0.10%";
        engFrequency[25]= "Z\t\t\t0.07%";


        return engFrequency;
    }
}
