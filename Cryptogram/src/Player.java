public class Player {
    private String userName;
    private int correctGuesses;
    private int totalGuesses;
    private double accuracy;
    private int cryptogramsPlayed;
    private int cryptogramsCompleted;

    public Player(String userName, int totalGuesses, int correctGuesses, int played, int completed){
        this.userName = userName;
        this.correctGuesses = correctGuesses;
        this.totalGuesses = totalGuesses;
        this.cryptogramsPlayed = played;
        this.cryptogramsCompleted = completed;
    }

    public String getUserName(){
        return userName;
    }

    public String toString() {
        return this.userName + " " + this.totalGuesses + " " + this.correctGuesses + " " + getNumCryptogramsPlayed() + " " + getNumCryptogramsCompleted();
    }

    public String getStats() {
        return "Player: " + this.userName + "\nTotal Guesses: " + this.totalGuesses + "\nCorrect Guesses: " + this.correctGuesses + "\nAccuracy:  " + getAccuracy() + "%\nCryptograms Played: " + getNumCryptogramsPlayed() + "\nCryptograms Completed: " + getNumCryptogramsCompleted();
    }

    public void incrementCryptogramsPlayed() {
        this.cryptogramsPlayed++;
    }

    public void incrementCryptogramsCompleted() {
        this.cryptogramsCompleted++;
    }

    public int getNumCryptogramsPlayed() {
        return this.cryptogramsPlayed;
    }

    public int getNumCryptogramsCompleted() {
        return this.cryptogramsCompleted;
    }

    public void incrementTotalGuesses() { this.totalGuesses++; }

    public void incrementCorrectGuesses() { this.correctGuesses++; }

    public int getTotalGuesses() { return this.totalGuesses; }

    public int getCorrectGuesses() { return this.correctGuesses; }

    public double getAccuracy() {
        if (this.totalGuesses == 0) return this.accuracy = 0;
        return this.accuracy = (this.correctGuesses*100/this.totalGuesses);
    }
}
